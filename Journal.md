# Milestone 1: Journal #

### Day One: ###

 Met with group discussed the assessment and chose what API we wanted, chose facebook api,
we also talked about what we wanted our website to do and chose instead of having to log in to facebook
to display your info we would just do a search bar to search facebook and display the results of everything
that has what you searched for and then you would chose one which would take you to shows all the public info
available. We also researched if this is possible and found a stackoverflow thread that said it is. We also 
said we would use slack to communicate and Nigel as our leader also introduces us to a organising tool called trello
which will be used for organising.

### Day Two: ###

 Looked into some resources provided by Jeff and also found some more my self, also made a wireframe of 
the 3 pages we decided on search, results and info display.

### Day Three: ###

 Had a short meeting with team and made sure we where happy with the pages and also what we had to do on 
the report, we also talked about having work and other assesments due so we just said we'd try our best to do as much 
we can but we were well aware of what we where doing for the API and what tools we'll use.

### Day Four: ###

 I started working on the report added the wireframe and added basic info and resources that i had found
I also made a trello board and added tasks for milestone 2 and things we had already done like making the repo.

### Day Five: ###

 Found out I didn't give edit rights to team mates but Nigel made a new one and added us we also chose to 
have a three view layout and use bootstrap for it.